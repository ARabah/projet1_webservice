<?php

namespace App\Repository;

use App\Entity\Categorie;
use App\Entity\Film;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Film>
 *
 * @method Film|null find($id, $lockMode = null, $lockVersion = null)
 * @method Film|null findOneBy(array $criteria, array $orderBy = null)
 * @method Film[]    findAll()
 * @method Film[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FilmRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry) { parent::__construct($registry, Film::class); }

    public function selectAll() : array {
        $conn = $this->getEntityManager()->getConnection();

        $sql = 'SELECT f.id, f.nom, f.description, f.date, f.note, c.libelle as catégirie
        FROM film f
        join categorie_film cf on cf.film_id = f.id
        join categorie c on c.id = cf.categorie_id';

        $resultSet = $conn->executeQuery($sql, []);

        // returns an array of arrays (i.e. a raw data set)
        return $resultSet->fetchAllAssociative();
    }


    public function selectBy(string $type, string $value) : array {
        $conn = $this->getEntityManager()->getConnection();
        

        $crits = [];
        $params = [];

        try {
            switch ($type) {
                case 'titre':
                    if (!empty($value)) {
                        foreach (explode(" ", $value) as $w) {
                            $params[] = "%".$w."%";
                            $crits[] = "UPPER(f.nom) like upper(:value" . count($params)-1 . ")";
                        }
                    }
                    else throw new \Exception("Il n'y a aucune valeur", 400); 
                    break;
                case 'description':
                    if (!empty($value)) {
                        foreach (explode(" ", $value) as $w) {
                            $params[] = "%".$w."%";
                            $crits[] = "UPPER(f.description) like upper(:value" . count($params) . ")";
                        }
                    }
                    else throw new \Exception("Il n'y a aucune valeur", 400); 
                    break;
                default: throw new \Exception("Il n'y a aucune valeur ou pas la bonne", 400); break;
            }


            $rsm = new ResultSetMappingBuilder($this->getEntityManager(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);


            $sql = "SELECT f.id, f.nom as titre, f.description, f.date, f.note, c.libelle as categorie from film f
            join categorie_film cf on cf.film_id = f.id
            join categorie c on c.id = cf.categorie_id
            " . (!empty($crits) ? "WHERE " . implode(" AND ", $crits) : "");
    
    
            $em = $this->getEntityManager();
    
            $res = $em->createNativeQuery($sql, $rsm)->setParameter("value0", $params[0]);
            // for ($i=0; $i < count($params); $i++) { 
            //     $res->setParameter("value".$i, $params[$i]);
            // }
            dd($res->getArrayResult());
            // return $res->getResult();









        } catch (\Exception $e) {
            return ['erreur' => $e->getMessage(), "code" => $e->getCode()];
        }

    }


    

    // voire ce lien si jamais ça n'a pas marché https://www.doctrine-project.org/projects/doctrine-orm/en/current/reference/native-sql.html#the-nativequery-class



//    /**
//     * @return Film[] Returns an array of Film objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('f.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Film
//    {
//        return $this->createQueryBuilder('f')
//            ->andWhere('f.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
