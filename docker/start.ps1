# Initialisation
$git_path = Read-Host "Entrez le chemin absolu vers le dossier du clone Git : "
$docker = "$env:ProgramFiles\docker"

Set-Location $docker

# Copier les fichiers default-compose.yml et .env.default, puis les renommer en compose.yml et .env
Copy-Item default-compose.yml compose.yml
Copy-Item .env.default .env

# Ouvrir le fichier .env et renseigner le port web souhaité et le chemin absolu vers le dossier du clone Git
$bdd_path = Read-Host "Entrez le chemin absolu vers le dossier de la base de donner ou du volume : "

# Remplacer les valeurs dans le fichier .env
(Get-Content .env) -replace 'WEB_VOLUME=', "WEB_VOLUME=$git_path" | Set-Content .env
(Get-Content .env) -replace 'DB_VOLUME=', "DB_VOLUME=$bdd_path" | Set-Content .env

# Créer le réseau Docker
docker network create ynov

# Installation des vendors
docker run --rm -t -u 1000 -v "${git_path}:/var/www/app" web composer install --no-cache

# Installation de la base de données
$database = docker container create -v "${bdd_path}:/var/lib/postgresql/data" -e POSTGRES_DB=izilease -e POSTGRES_INITDB_ARGS="--locale-provider=icu --icu-locale=fr-FR" -e POSTGRES_PASSWORD=ynov -e POSTGRES_USER=ynov -e TZ=Europe/Paris postgres:15
docker start $database

# Attente que PostgreSQL soit prêt
do {
    Start-Sleep -Seconds 1
} until ((docker exec $database pg_isready) -eq 0)

Write-Host "PostgreSQL est prêt."

docker exec -itu postgres $database psql -c "CREATE DATABASE ynov WITH OWNER ynov"
docker cp "$docker\Film+API.sql" "${database}:/resource.sql"
docker exec -itu postgres $database psql -U ynov ynov -f resource.sql
docker exec $database rm resource.sql
docker stop $database
docker rm $database

# Dans le dossier docker, lancer docker-compose up pour démarrer le composeur ou, si vous ne souhaitez pas attacher le terminal à l'image Docker, utilisez docker-compose up -d
# docker-compose up -d ou docker-compose up 
