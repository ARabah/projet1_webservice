--
-- PostgreSQL database dump
--

-- Dumped from database version 15.5 (Debian 15.5-1.pgdg120+1)
-- Dumped by pg_dump version 15.5 (Debian 15.5-1.pgdg120+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: categorie; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.categorie (
    id integer NOT NULL,
    libelle character varying(255) NOT NULL
);


ALTER TABLE public.categorie OWNER TO ynov;

--
-- Name: categorie_film; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.categorie_film (
    categorie_id integer NOT NULL,
    film_id integer NOT NULL
);


ALTER TABLE public.categorie_film OWNER TO ynov;

--
-- Name: categorie_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.categorie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categorie_id_seq OWNER TO ynov;

--
-- Name: doctrine_migration_versions; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.doctrine_migration_versions (
    version character varying(191) NOT NULL,
    executed_at timestamp(0) without time zone DEFAULT NULL::timestamp without time zone,
    execution_time integer
);


ALTER TABLE public.doctrine_migration_versions OWNER TO ynov;

--
-- Name: film; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.film (
    id integer NOT NULL,
    nom character varying(128) NOT NULL,
    description character varying(2048) NOT NULL,
    date date NOT NULL,
    note smallint NOT NULL
);


ALTER TABLE public.film OWNER TO ynov;

--
-- Name: film_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.film_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.film_id_seq OWNER TO ynov;

--
-- Name: token; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.token (
    id integer NOT NULL,
    utilisateur_id integer,
    access_token character varying(255) DEFAULT NULL::character varying,
    access_token_expires_at date,
    refresh_token character varying(255) NOT NULL,
    refresh_token_expires_at date
);


ALTER TABLE public.token OWNER TO ynov;

--
-- Name: token_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.token_id_seq OWNER TO ynov;

--
-- Name: utilisateur; Type: TABLE; Schema: public; Owner: ynov
--

CREATE TABLE public.utilisateur (
    id integer NOT NULL,
    uid uuid NOT NULL,
    login character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    roles json NOT NULL,
    status character varying(255) NOT NULL,
    create_at timestamp(0) without time zone NOT NULL,
    updated_at timestamp(0) without time zone NOT NULL
);


ALTER TABLE public.utilisateur OWNER TO ynov;

--
-- Name: COLUMN utilisateur.create_at; Type: COMMENT; Schema: public; Owner: ynov
--

COMMENT ON COLUMN public.utilisateur.create_at IS '(DC2Type:datetime_immutable)';


--
-- Name: COLUMN utilisateur.updated_at; Type: COMMENT; Schema: public; Owner: ynov
--

COMMENT ON COLUMN public.utilisateur.updated_at IS '(DC2Type:datetime_immutable)';


--
-- Name: utilisateur_id_seq; Type: SEQUENCE; Schema: public; Owner: ynov
--

CREATE SEQUENCE public.utilisateur_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.utilisateur_id_seq OWNER TO ynov;

--
-- Data for Name: categorie; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.categorie (id, libelle) FROM stdin;
1	action
2	fantésie
\.


--
-- Data for Name: categorie_film; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.categorie_film (categorie_id, film_id) FROM stdin;
2	1
1	1
1	2
\.


--
-- Data for Name: doctrine_migration_versions; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.doctrine_migration_versions (version, executed_at, execution_time) FROM stdin;
\.


--
-- Data for Name: film; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.film (id, nom, description, date, note) FROM stdin;
1	Inception	Un thriller de science-fiction sur les rêves.	2010-07-16	5
2	Forrest Gump	L'histoire d'un homme avec un QI inférieur à la moyenne qui réussit dans la vie.	1994-07-06	4
3	The Dark Knight	Un film de super-héros basé sur le personnage de Batman.	2008-07-18	5
4	Pulp Fiction	Une série d'histoires interconnectées dans le monde du crime.	1994-05-21	4
5	La La Land	Une romance musicale sur deux aspirants artistes à Los Angeles.	2016-08-31	3
6	test	testdescription	2023-12-11	5
\.


--
-- Data for Name: token; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.token (id, utilisateur_id, access_token, access_token_expires_at, refresh_token, refresh_token_expires_at) FROM stdin;
5	\N	eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5NGUyY2U3Ni1hNDE0LTQ0ZGMtYmQyZi05OGViZmFhMmIyMjIiLCJpYXQiOiIxNzAyOTk5NjA3In0.td049P9j7lypj1YB73xG7NnOXifVb66E4_TE0vjV-DI	2023-12-19	eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiI5NGUyY2U3Ni1hNDE0LTQ0ZGMtYmQyZi05OGViZmFhMmIyMjIiLCJpYXQiOiIxNzAzMDAzMjA3In0.Oh0cHlBDlYS_WbM9XRZoeydNjAEdXxzdXcxPNfcHlPI	2023-12-19
\.


--
-- Data for Name: utilisateur; Type: TABLE DATA; Schema: public; Owner: ynov
--

COPY public.utilisateur (id, uid, login, password, roles, status, create_at, updated_at) FROM stdin;
\.


--
-- Name: categorie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.categorie_id_seq', 2, true);


--
-- Name: film_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.film_id_seq', 6, true);


--
-- Name: token_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.token_id_seq', 5, true);


--
-- Name: utilisateur_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ynov
--

SELECT pg_catalog.setval('public.utilisateur_id_seq', 1, false);


--
-- Name: film Film_id; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.film
    ADD CONSTRAINT "Film_id" PRIMARY KEY (id);


--
-- Name: categorie_film categorie_film_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.categorie_film
    ADD CONSTRAINT categorie_film_pkey PRIMARY KEY (categorie_id, film_id);


--
-- Name: categorie categorie_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.categorie
    ADD CONSTRAINT categorie_pkey PRIMARY KEY (id);


--
-- Name: doctrine_migration_versions doctrine_migration_versions_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.doctrine_migration_versions
    ADD CONSTRAINT doctrine_migration_versions_pkey PRIMARY KEY (version);


--
-- Name: token token_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.token
    ADD CONSTRAINT token_pkey PRIMARY KEY (id);


--
-- Name: utilisateur utilisateur_pkey; Type: CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.utilisateur
    ADD CONSTRAINT utilisateur_pkey PRIMARY KEY (id);


--
-- Name: idx_f56d3975567f5183; Type: INDEX; Schema: public; Owner: ynov
--

CREATE INDEX idx_f56d3975567f5183 ON public.categorie_film USING btree (film_id);


--
-- Name: idx_f56d3975bcf5e72d; Type: INDEX; Schema: public; Owner: ynov
--

CREATE INDEX idx_f56d3975bcf5e72d ON public.categorie_film USING btree (categorie_id);


--
-- Name: uniq_5f37a13bfb88e14f; Type: INDEX; Schema: public; Owner: ynov
--

CREATE UNIQUE INDEX uniq_5f37a13bfb88e14f ON public.token USING btree (utilisateur_id);


--
-- Name: token fk_5f37a13bfb88e14f; Type: FK CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.token
    ADD CONSTRAINT fk_5f37a13bfb88e14f FOREIGN KEY (utilisateur_id) REFERENCES public.utilisateur(id);


--
-- Name: categorie_film fk_f56d3975567f5183; Type: FK CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.categorie_film
    ADD CONSTRAINT fk_f56d3975567f5183 FOREIGN KEY (film_id) REFERENCES public.film(id) ON DELETE CASCADE;


--
-- Name: categorie_film fk_f56d3975bcf5e72d; Type: FK CONSTRAINT; Schema: public; Owner: ynov
--

ALTER TABLE ONLY public.categorie_film
    ADD CONSTRAINT fk_f56d3975bcf5e72d FOREIGN KEY (categorie_id) REFERENCES public.categorie(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

