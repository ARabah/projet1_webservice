@echo off
REM Initialisation
set /p git_path="Entrez le chemin absolu vers le dossier du clone Git : "
set docker="%git_path%\docker"

cd %ddocker%

REM Copier les fichiers default-compose.yml et .env.default, puis les renommer en compose.yml et .env
copy default-compose.yml compose.yml
copy .env.default .env


REM Ouvrir le fichier .env et renseigner le port web souhaité et le chemin absolu vers le dossier du clone Git
set /p bdd_path="Entrez le chemin absolu vers le dossier de la base de donner ou du volume : "

REM Remplacer les valeurs dans le fichier .env
powershell -Command "(Get-Content .env) -replace 'WEB_VOLUME=', 'WEB_VOLUME=""%git_path%"\"' | Set-Content .env"
powershell -Command "(Get-Content .env) -replace 'DB_VOLUME=', 'DB_VOLUME=""%bdd_path%"\"' | Set-Content .env"

cd %git_path%

REM Créer le réseau Docker
docker network create ynov

REM Création des images docker
docker image build -t ynov:symfony6 "%docker%\web"
docker image build -t ynov:postgres-15 "%docker%\postgres"

REM Installation des vendors
docker run --rm -t -u 1000 -v %git_path%:/var/www/app ynov:symfony6 composer install --no-cache

REM Installation de la base de données
for /f %%i in ('docker container create -v "%bdd_path%:/var/lib/postgresql/data" -e "POSTGRES_DB=ynov" -e "POSTGRES_INITDB_ARGS=--locale-provider=icu --icu-locale=fr-FR" -e "POSTGRES_PASSWORD=ynov" -e "POSTGRES_USER=ynov" -e "TZ=Europe/Paris" ynov:postgres-15') do set "database=%%i"
docker start %database%

:CHECK_READY
docker exec %database% pg_isready

REM vérifie si la base de donner et ouvert
if not %errorlevel% equ 0 (
    echo docker exec %database% pg_isready
    echo en attente que PostgreSQL sois pres...
    timeout /nobreak /t 1 >nul
    goto CHECK_READY
)

echo PostgreSQL est pres.

docker exec -itu postgres %database% psql -c "CREATE DATABASE ynov WITH OWNER ynov"
docker cp %docker%/Film+API.sql %database%:/resource.sql
docker exec -itu postgres %database% psql -U ynov ynov -f resource.sql
docker exec %database% rm resource.sql
docker stop %database%
docker rm %database%

REM Dans le dossier docker, lancer docker compose up pour démarrer le composeur ou, si vous ne souhaitez pas attacher le terminal à l'image Docker, utilisez docker compose up -d
REM docker compose up -d ou docker compose up 
