const express = require('express')
const CFilms  = require('./class/class_films')
const fs = require('fs');
const convert = require('xml-js')


const app = express()
const port = 80

app.get('/format', (req, res) => {
  res.json({nom: '', description: '', date: '{date}', note: 'int'})
})

app.get("/films", (req, res) => {
    let content_type = req.headers['content-type']

    fs.readFile('./database.json', 'utf8', (err, fdata) => {
        if (err) {
            console.error(err);
            return;
        }
    
        // Process the file data here
        let options = {compact: true, ignoreComment: true, spaces: 4};
        switch (content_type) {
            case 'application/json': res.status(200).send(JSON.parse(fdata))
            case 'application/xml': res.status(200).send(convert.json2xml(fdata, options))
        }
    });
})


app.route("/film/:id")
    .get((req, res) => {
        let content_type = req.headers['content-type']
        
        fs.readFile('./database.json', 'utf8', (err, fdata) => {
            if (err) {
                console.error(err);
                return;
            }
            let options = {compact: true, ignoreComment: true, spaces: 4};
            let data = JSON.parse(fdata).find(fd => fd.id == req.params.id)
            if (data == undefined) { res.status(422).send('Resource Introuvable') }
            // Process the file data here
            switch (content_type) {
                case 'application/json': res.send(data); break;
                case 'application/xml': res.send(convert.json2xml(data, options)); break;
                default: res.status(422).send("Le content-type doit etre 'application/json' ou 'application/xml'")
            }
        });
    })
    .delete((req, res) => {
        fs.readFile('./database.json', 'utf8', (err, fdata) => {
            if (err) {
                console.error(err);
                return;
            }
            let index = JSON.parse(fdata).findIndex(fd => fd.id == req.params.id)
            let data = JSON.parse(fdata)[index]

            if (data == undefined) { res.status(422).send('Resource Introuvable') }

            fs.writeFile('./database.json', JSON.stringify(JSON.parse(fdata).filter(fd => fd.id != req.params.id)), 'utf8', (err) => {
                if (err) { res.status(404).send('Erreur') } 
                else { res.status(200).send('Resource Modifier') }
            })
            
        });
    })

app.route("/film/creation/:nom/:description/:date/:note")
    .post((req, res) => {

        function length(d) {
            return d.length
        }

        fs.readFile('./database.json', 'utf8', (err, fdata) => {
            if (err) {
                console.error(err);
                return;
            }


            let data = JSON.parse(fdata)
            let date = new Date(req.params.date)
            let index = length(data)
           


            data.push({
                "id": index+2,
                "nom": req.params.nom,
                "description": req.params.description,
                "date": date.toISOString(),
                "note": req.params.note
            })


            res.json(data)


            fs.writeFile('./database.json', JSON.stringify(data), 'utf8', (err) => {
                if (err) { res.status(404).send('Erreur') } 
                else { res.status(201).send('Resource Crée') }
            })
            
        });

    })

app.route("/film/:id/modif/:nom/:description/:date/:note")
    .put((req, res) => {
        fs.readFile('./database.json', 'utf8', (err, fdata) => {
            if (err) {
                console.error(err);
                return;
            }



            let data = JSON.parse(fdata)
            let index = data.findIndex(fd => fd.id == req.params.id)
            
            if (data[index] == undefined) { res.status(422).send('Resource Introuvable') }
            
            let date = new Date(req.params.date == null ? '0000-00-00': req.params.date)


            data[index].nom = req.params.nom
            data[index].description = req.params.description
            data[index].date = req.params.date == null ? null : date
            data[index].note = req.params.note
            
            fs.writeFile('./database.json', JSON.stringify(data), 'utf8', (err) => {
                if (err) { res.status(404).send('Erreur') } 
                else { res.status(201).send('Resource Modifée') }
            })
        });
    })
    
    .patch((req, res) => {
        fs.readFile('./database.json', 'utf8', (err, fdata) => {
            if (err) {
                console.error(err);
                return;
            }



            let data = JSON.parse(fdata)
            let index = data.findIndex(fd => fd.id == req.params.id)
            
            if (data[index] == undefined) { res.status(422).send('Resource Introuvable') }
            
            let date = new Date(req.params.date == null ? data[index].date : req.params.date)


            data[index].nom = req.params.nom == null ? data[index].nom : req.params.nom
            data[index].description = req.params.description == null ? data[index].description : req.params.description
            data[index].date = req.params.date == null ? data[index].date : date
            data[index].note = req.params.note == null ? data[index].note : req.params.note
            
            fs.writeFile('./database.json', JSON.stringify(data), 'utf8', (err) => {
                if (err) { res.status(404).send('Erreur') } 
                else { res.status(201).send('Resource Modifée') }
            })
        });    
    })








app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
