<?php

require_once(__DIR__ . '/../bricabrac.inc.php');
require_once(__DIR__ . '/class_dev_sql.php');

class CFilms {

    private $_pt = null;

    private $m_method = null;
    private $m_contenteType = null;
    
    public function __construct($_method = null, $_contenteType = null) {
        $this->_pt = new dev_SQL();
        $this->m_method = $_method;
        $this->m_contenteType = $_contenteType;
    }
    public function GetFilms($_props = null): bool|array {
        $query = "SELECT * from Film";
        $res = $this->_pt->loadArray($query);

        return $res;
        // $array = array(
        //     "test" => 'td'
        // );

        // if ($_props == null || !is_array($_props)) {
        //     return $array;
        // }
        // else {
        //     return filterArrayProps($array, $_props);
        // }
    }
    public function Film($description = null, $date = null, $note = null, $name = null) {
        $array = array();

        $name = ($name === null) ? '' : $name = 'nom = ' . $name;
        $note = ($note === null) ? '' : $note = 'note = ' . $note;
        $date = ($date === null) ? '' : 'date = ' . $date;
        $description = ($description === null) ? '' : 'description = ' . $description;

        $req = sprintf(
            'SELECT
                id
            FROM 
                Film
            WHERE 
                %s',
            //RAF trouver la gymnastique pour pouvoir le faire sans avoir le pb de "where (null) and (null) and 2012-10-10 and (null)

        )

        else {
            return filterArrayProps($array, $_props);
        }
    }
}