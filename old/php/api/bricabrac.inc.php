<?php

function filterArrayProps($_array = array(), $_props = array()) {
    if (!is_array($_array) || !is_array($_props)) return false;

    $result = array();

    foreach ($_props as $prop) {
        $result[$prop] = isset($_array[$prop]) ? $_array[$prop] : null;
    }

    return $result;
}