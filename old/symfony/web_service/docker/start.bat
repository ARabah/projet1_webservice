docker compose exec -u postgres database psql -c "CREATE USER \"user\" WITH PASSWORD 'user'"
docker compose exec -u postgres database psql -c "CREATE DATABASE web_service WITH OWNER \"user\""

docker compose cp Film.sql database:Film.sql
docker compose exec -u postgres database psql web_service -f Film.sql
docker compose exec database rm Film.sql