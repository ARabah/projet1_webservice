const { Client } = require('pg')


class dev_SQL {
    #client = new Client({
        host: 'database',
        port: 5432,
        database: 'web_service',
        user: 'postgres',
        password: 'web_service',
      })
    constructor() {
        this.connect()
    }

    _isArray(Array) {
        return Array.isArray(Array)
    }
    
    async connect() {
        this.#client.connect()
    }

    async query(query) {
        try {
            return await this.#client.query(query);
        } catch (error) {
            return error;
        } finally {
            this.#client.end();
        }
    }

    async queryParams(query, params) {
        try {
            if (this._isArray(params)) {
                return await this.#client.query(query, params);
            } else {
                throw new Error(JSON.stringify({ error: 'Le paramètre n\'est pas un tableau', code: 404 }));
            }
        } catch (error) {
            return error;
        } finally {
            this.#client.end();
        }
    }

    async loadData(query, params) {
        if (query === "" || query === undefined) {
            try {
                if (this._isArray(params)) {
                    let res;
                    if (params !== null || params !== undefined) { res = await this.#client.queryParams(query, params); } 
                    else { res = await this.#client.query(query); }
                    if (res === false) return res
                    return res.rows[0]
                } else { throw new Error(JSON.stringify({ error: 'Le paramètre n\'est pas un tableau', code: 404 })); }
            } catch (error) { return error; } 
            finally { this.#client.end(); }
        } else { throw new Error(JSON.stringify({ error: 'L\'argument query est vide', code: 404 }));}
    }
    
    async loadArray(query, params) {
        if (query !== "" || query !== undefined) {
            try {
                if (this._isArray(params)) {
                    let res;
                    if (params !== null || params !== undefined) { res = await this.#client.queryParams(query, params); } 
                    else { res = await this.#client.query(query); }
                    if (res === false) { return res }
                    return res.rows;
                } else { throw new Error(JSON.stringify({ error: 'Le paramètre n\'est pas un tableau', code: 404 })); }
            } 
            catch (error) { return error; } 
            finally { this.#client.end(); }
        } else {
            throw new Error(JSON.stringify({ error: 'L\'argument query est vide', code: 404 }));
        }
    }

}

module.exports = dev_SQL