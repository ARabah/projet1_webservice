# Service Web de Gestion de Films

## Réalisé par

- ASSEUM Rabah
- RICOTTA Giovanni

## Installation avec Docker

1. Accédez au dossier *docker*.
2. Copiez les fichiers **default-compose.yml** et **.env.default**, puis renommez-les en ***compose.yml*** et ***.env***.
3. Ouvrez le fichier **.env** et renseignez le port web souhaité ainsi que le chemin absolu vers le dossier du clone Git dans :
    - WEB_VOLUME
    - DB_VOLUME
4. Exécutez la commande suivante : **docker network create ynov**
5. Dans le dossier docker, lancez `docker compose up` pour démarrer le composeur, et `docker compose up -d` si vous ne souhaitez pas attacher le terminal à l'image Docker.

> **NOTE** 
> Lancez le script start.bat ou start.ps1. En cas d'échec, suivez simplement les étapes du script.


<!-- ## Démarrage sans Docker

1. Dans le dossier du projet, ouvrez un terminal.
2. Exécutez `npm i` pour installer les dépendances JS.
3. Ensuite, lancez `npm run run` pour démarrer le projet.
4. Accédez au lien suivant : **<http://localhost:80>** -->

## Directives d'utilisation

### GET /film/:id

1. Description

Permet de récupérer les informations d'un film spécifié par son identifiant (id).

2. Paramètres

> id (obligatoire) : L'identifiant unique du film.

3. Réponse

La réponse peut être obtenue au format JSON ou XML, selon l'en-tête "Content-Type" de la requête.

Format JSON:
```json
{
  "id": "ID du film",
  "nom": "Nom du Film",
  "description": "Description du film",
  "date": "Date du film",
  "note": "Note du Film"
}
```

Format HAL+JSON:
```json
{
  "links": {
    "href": "/route",
    "rel": "entité",
    "type": "méthode"
  },
  "id": "ID du film",
  "nom": "Nom du Film",
  "description": "Description du film",
  "date": "Date du film",
  "note": "Note du Film"
}
```

Format XML:
```xml
<?xml version="1.0"?>
<films>
  <id>: ID du film <id>
  <nom>: Nom du Film <nom>
  <description>: Description du film <description>
  <date>: Date du film <date>
  <note>: Note du Film <note>
</films>
```

Format HAL+XML:
```xml
<?xml version="1.0"?>
<films>
  <_links>
    <href>/route</href>
    <rel>entité</rel>
    <type>méthode</type>
  </_links>
  <id>: ID du film <id>
  <nom>: Nom du Film <nom>
  <description>: Description du film <description>
  <date>: Date du film <date>
  <note>: Note du Film <note>
</films>
```

4. Retours Possibles

404 Not Found
    Ressource non trouvée.

Statut HTTP 200 :
> OK

Statut HTTP 404 :
> Not Found

---

### DELETE /film/:id

1. Description

Cet endpoint permet de supprimer les informations d'un film spécifié par son identifiant (id).

2. Paramètres

> id (obligatoire) : L'identifiant unique du film à supprimer.

3. Réponse

404 Not Found
    Ressource non trouvée.

Statut HTTP 200 
> OK

Statut HTTP 404
> Not Found

---

### GET /films

1. Description

Permet de récupérer la liste complète des films disponibles.

2. Réponse

La réponse peut être obtenue au format JSON ou XML, selon l'en-tête "Accept" de la requête.

Format JSON:
```json
[
 {
   "id": "ID du film 1",
   "nom": "Nom du film 1",
   "description": "Description du film 1",
   "date": "Date du film",
   "note": "Note du film 1"
 }
 {
   "id": "ID du film 2",
   "nom": "Nom du film 2",
   "description": "Description du film 2",
   "date": "Date du film",
   "note": "Note du film 2"
 }
]
```

Format XML:

```xml
<0>
	<id>: ID du film 1 <id>
   <nom>: Nom du film 1 <nom>
   <description>: Description du film 1 <description>
   <date>: Date du film <date>
   <note>: Note du film 1 <note>
</0>
<1>
	<id>: ID du film 2 <id>
   <nom>: Nom du film 2 <nom>
   <description>: Description du film 2 <description>
   <date>: Date du film <date>
   <note>: Note du film 2 <note>
</1>
```

3. Retours Possibles

404 Not Found
    Ressource non trouvée.

Statut HTTP 200 
> OK

Statut HTTP 404
> Not Found

---

### GET /format

1. Description

Cet endpoint permet de récupérer le format JSON des informations sur les films.

### POST /film/creation/:nom/:description/:date/:note

1. Description

Permet de créer un nouvel objet.

2. Paramètres

> nom (obligatoire) : nom du film.

> description (obligatoire) : description du film.

> date (obligatoire) : date de parution du film.

> note (obligatoire) : note attribuée au film.

> id (obligatoire) : L'identifiant unique du film à supprimer.

3. Réponse

404 Not Found
    Ressource non trouvée.

Statut HTTP 201 
> Création réussie

Statut HTTP 404
> Not Found

---

### PUT/PATCH /film/:id/modif/:nom/:description/:date/:note

1. Description

Permet de modifier un objet.

2. Paramètres

> id (obligatoire) : L'identifiant unique du film.

> nom (pas obligatoire) : nom du film, mettre la valeur **null** si aucune modification n'est souhaitée avec la méthode **PATCH**.

> description (pas obligatoire) : description du film, mettre la valeur **null** si aucune modification n'est souhaitée avec la méthode **PATCH**.

> date (pas obligatoire) : date de parution du film, mettre la valeur **null** si aucune modification n'est souhaitée avec la méthode **PATCH**.

> note (pas obligatoire) : note attribuée au film, mettre la valeur **null** si aucune modification n'est souhaitée avec la méthode **PATCH**.

3. Réponse

Statut HTTP 200 
> Ressource modifiée

Statut HTTP 422
> Ressource introuvable

---

### GET /film/recherche/:typeRecherche/:value

1. Description

Permet de rechercher un film par son titre ou sa description.

2. Paramètres

> typeRecherche (obligatoire) : doit être soit **titre** ou **description**.

> value (obligatoire) : texte à rechercher.

3. Réponse

Statut HTTP 200 
> Ressource trouvée

Statut HTTP 422
> Ressource introuvable